PROJECT_NAME := "docker-stack-wait"
PKG := "./"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	@go get github.com/mgechev/revive
	@revive --formatter friendly -exclude vendor/... ./... ${PKG_LIST}

test: ## Run unittests
	@go get -u github.com/jstemmer/go-junit-report
	@go test -v ${PKG_LIST} 2>&1 | go-junit-report > report.xml

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

dep: ## Get the dependencies
	@go mod download
	@go mod vendor -v

build: dep test ## Build the binary file
	@CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-w' -v $(PKG)

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
